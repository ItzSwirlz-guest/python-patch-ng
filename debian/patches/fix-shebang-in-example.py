Fix python shebang

This reproduced a bug in lintian that makes it ignore /usr/bin/env, and thus
making lintian read it as '#!python'.

Normally a lintian override would be fine, especially for a pedantic issue
but currently patch-ng works with both Python 2 and 3, and the developers
in their more popular projects, such as conan has already ended Python 2
support. This also follows developer standards and encourages Python 3 use.
Index: python-patch-ng-1.17.4/tests/06nested/examples/font_comparison.py
===================================================================
--- python-patch-ng-1.17.4.orig/tests/06nested/examples/font_comparison.py
+++ python-patch-ng-1.17.4/tests/06nested/examples/font_comparison.py
@@ -1,4 +1,4 @@
-#!/usr/bin/env python
+#!/usr/bin/env python3
 # ----------------------------------------------------------------------------
 # pyglet
 # Copyright (c) 2006-2008 Alex Holkner
